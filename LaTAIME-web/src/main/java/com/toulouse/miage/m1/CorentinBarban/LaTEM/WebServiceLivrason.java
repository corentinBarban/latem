/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.toulouse.miage.m1.CorentinBarban.LaTEM;

import com.toulouse.miage.m1.corentinbarban.LaTAIME.business.GestionLivraisonLocal;
import com.toulouse.miage.m1.corentinbarban.LaTAIME.entities.Commande;
import exceptions.TourneeNullException;
import java.util.Collection;
import javax.ejb.EJB;
import javax.jws.WebMethod;
import javax.jws.WebService;

/**
 * WebService Livraison
 */
@WebService(serviceName = "WebServiceLivrason")
public class WebServiceLivrason {

    @EJB
    private GestionLivraisonLocal ejbRef;
    
    
    /**
     * Etablir la liste des commande à livrer
     * @return
     * @throws TourneeNullException 
     */
    @WebMethod(operationName = "etablirTournee")
    public Collection<Commande> etablirTournee() throws TourneeNullException {
        return ejbRef.etablirTournee();
    }
    
}
