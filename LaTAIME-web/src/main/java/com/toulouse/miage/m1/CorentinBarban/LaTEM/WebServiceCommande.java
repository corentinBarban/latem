/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.toulouse.miage.m1.CorentinBarban.LaTEM;

import com.toulouse.miage.m1.corentinbarban.LaTAIME.business.GestionCommandeLocal;
import com.toulouse.miage.m1.corentinbarban.LaTAIME.entities.Client;
import com.toulouse.miage.m1.corentinbarban.LaTAIME.entities.Produit;
import exceptions.CommandeAcquiterException;
import exceptions.CommandeCloturerException;
import exceptions.CommandeInexistanteException;
import exceptions.QuantiteInsuffisanteException;
import java.util.Collection;
import java.util.HashMap;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 * WebService Commande
 */
@WebService(serviceName = "WebServiceCommande")
@Stateless()
public class WebServiceCommande {

    @EJB
    private GestionCommandeLocal ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Web Service > Add Operation"
    
    /**
     * valider un panier
     * @param listeProduits
     * @param idClient
     * @return 
     */
    @WebMethod(operationName = "validerPanier")
    public String validerPanier(@WebParam(name = "listeProduits") HashMap<Integer, Integer> listeProduits, @WebParam(name = "idClient") Integer idClient) {
        return ejbRef.validerPanier(listeProduits, idClient);
    }
    
    /**
     * valider une commande
     * @param idCommande
     * @return
     * @throws CommandeInexistanteException
     * @throws QuantiteInsuffisanteException
     * @throws CommandeAcquiterException 
     */
    @WebMethod(operationName = "validerCommande")
    public String validerCommande(@WebParam(name = "idCommande") Integer idCommande) throws CommandeInexistanteException, QuantiteInsuffisanteException,CommandeAcquiterException {
        return ejbRef.validerCommande(idCommande);
    }
    
    /**
     * Cloturer une commande
     * @param idCommande
     * @return
     * @throws exceptions.CommandeInexistanteException
     * @throws exceptions.CommandeCloturerException
     */
    @WebMethod(operationName = "cloturerCommande")
    public String cloturerCommande(@WebParam(name = "idCommande") Integer idCommande) throws CommandeInexistanteException, CommandeCloturerException  {
        return ejbRef.cloturerCommande(idCommande);
    }
    
    /**
     * Obtenir le statut de la commande
     * 
     * @param idCommande
     * @return String
     * @throws CommandeInexistanteException
     */
    @WebMethod(operationName = "getStatutCommande")
    public String getStatutCommande(@WebParam(name = "idCommande") Integer idCommande) throws CommandeInexistanteException{
        return ejbRef.getStatutCommande(idCommande);
    }
}
