/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.toulouse.miage.m1.CorentinBarban.LaTEM;

import com.toulouse.miage.m1.corentinbarban.LaTAIME.business.GestionCommercialLocal;
import com.toulouse.miage.m1.corentinbarban.LaTAIME.entities.Fournisseur;
import com.toulouse.miage.m1.corentinbarban.LaTAIME.entities.Produit;
import exceptions.FournisseurInexistantException;
import exceptions.ProduitInexistantException;
import java.util.Collection;
import java.util.HashMap;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 * WebService Commande fournisseur
 * 
 */
@WebService(serviceName = "WebServiceCommandeFournisseur")
@Stateless()
public class WebServiceCommandeFournisseur {

    @EJB
    private GestionCommercialLocal ejbRef;
    
    /**
     * Commander un fournisseur
     * @param produits
     * @param idFournisseur
     * @return
     * @throws ProduitInexistantException
     * @throws FournisseurInexistantException 
     */
    @WebMethod(operationName = "commandeFournisseur")
    public String commandeFournisseur(@WebParam(name = "produits") HashMap<Integer, Integer> produits, @WebParam(name = "idFournisseur") Integer idFournisseur) throws ProduitInexistantException, FournisseurInexistantException {
        return ejbRef.commandeFournisseur(produits, idFournisseur);
    }

    /**
     * Lister les fournisseurs
     * @return 
     */
    @WebMethod(operationName = "getFournisseurs")
    public Collection<Fournisseur> getFournisseurs() {
        //TODO write your implementation code here:
        return ejbRef.getFournisseurs();
    }
    
}
