/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.toulouse.miage.m1.CorentinBarban.LaTEM;

import com.toulouse.miage.m1.corentinbarban.LaTAIME.business.GestionProduitLocal;
import com.toulouse.miage.m1.corentinbarban.LaTAIME.entities.Produit;
import java.util.Collection;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.ejb.Stateless;
import javax.jws.WebMethod;

/**
 * WebService produit
 */
@WebService(serviceName = "WebServiceProduit")
@Stateless()
public class WebServiceProduit {

    @EJB
    private GestionProduitLocal ejbRef;
    
    /**
     * Lister l'ensemble des produits
     * @return collection de produits
     */
    @WebMethod(operationName = "getProduits")
    public Collection<Produit> getProduits() {
        return ejbRef.getProduits();
    }
    
}
