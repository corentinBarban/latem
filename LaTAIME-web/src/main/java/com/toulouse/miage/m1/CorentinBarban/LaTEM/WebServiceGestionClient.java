/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.toulouse.miage.m1.CorentinBarban.LaTEM;

import com.toulouse.miage.m1.corentinbarban.LaTAIME.business.GestionClientLocal;
import com.toulouse.miage.m1.corentinbarban.LaTAIME.entities.Client;
import exceptions.ClientInexistantException;
import exceptions.MotDePasseIncorrectException;
import java.util.Collection;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 * WebService Client
 */
@WebService(serviceName = "WebServiceGestionClient")
@Stateless()
public class WebServiceGestionClient {

    @EJB
    private GestionClientLocal ejbRef;
    
    /**
     * Authentification d'un client
     * @param identifiant
     * @param motDePasse
     * @return
     * @throws ClientInexistantException
     * @throws MotDePasseIncorrectException 
     */
    @WebMethod(operationName = "authentifierClient")
    public Client authentifierClient(@WebParam(name = "identifiant") String identifiant, @WebParam(name = "motDePasse") String motDePasse) throws ClientInexistantException, MotDePasseIncorrectException {
        return ejbRef.authentifierClient(identifiant, motDePasse);
    }

    /**
     * Lister l'ensemble des clients 
     * 
     * @return collection de client 
     */
    @WebMethod(operationName = "getClients")
    public Collection<Client> getClients() {
        return ejbRef.getClients();
    }
    
}
