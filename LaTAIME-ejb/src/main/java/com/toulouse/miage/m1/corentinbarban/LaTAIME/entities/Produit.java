/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.toulouse.miage.m1.corentinbarban.LaTAIME.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *Classe d'un produit
 */
@Entity
@Table(name = "PRODUIT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Produit.findAll", query = "SELECT p FROM Produit p")
    , @NamedQuery(name = "Produit.findByIdproduit", query = "SELECT p FROM Produit p WHERE p.idproduit = :idproduit")
    , @NamedQuery(name = "Produit.findByReference", query = "SELECT p FROM Produit p WHERE p.reference = :reference")
    , @NamedQuery(name = "Produit.findByDescription", query = "SELECT p FROM Produit p WHERE p.description = :description")
    , @NamedQuery(name = "Produit.findByPrixht", query = "SELECT p FROM Produit p WHERE p.prixht = :prixht")
    , @NamedQuery(name = "Produit.findByType", query = "SELECT p FROM Produit p WHERE p.type = :type")
    , @NamedQuery(name = "Produit.findByQuantitestock", query = "SELECT p FROM Produit p WHERE p.quantitestock = :quantitestock")
    , @NamedQuery(name = "Produit.findByTaux", query = "SELECT p FROM Produit p WHERE p.taux = :taux")})
public class Produit implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IDPRODUIT")
    private Integer idproduit;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "REFERENCE")
    private String reference;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "DESCRIPTION")
    private String description;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PRIXHT")
    private double prixht;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "TYPE")
    private String type;
    @Basic(optional = false)
    @NotNull
    @Column(name = "QUANTITESTOCK")
    private int quantitestock;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TAUX")
    private double taux;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "produit")
    private Collection<Commanderf> commanderfCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "produit")
    private Collection<Commanderc> commandercCollection;
    
    /**
     * Constructeur vide d'un produit
     */
    public Produit() {
    }
    
    /**
     * Constructeur d'un produit
     * @param idproduit
     */
    public Produit(Integer idproduit) {
        this.idproduit = idproduit;
    }
    
    /**
     * Constructeur d'un produit
     * @param idproduit Integer du produit
     * @param reference String reference 
     * @param description String
     * @param prixht Double
     * @param type String
     * @param quantitestock Integer
     * @param taux Double
     */
    public Produit(Integer idproduit, String reference, String description, double prixht, String type, int quantitestock, double taux) {
        this.idproduit = idproduit;
        this.reference = reference;
        this.description = description;
        this.prixht = prixht;
        this.type = type;
        this.quantitestock = quantitestock;
        this.taux = taux;
    }
    
    /**
     * Obtenir l'id d'un produit
     * @return Intger id du produit
     */
    public Integer getIdproduit() {
        return idproduit;
    }

    /**
     * Modifier l'id d'un produit
     * @param idproduit Integer
     */
    public void setIdproduit(Integer idproduit) {
        this.idproduit = idproduit;
    }
    
    /**
     * Obtenir la reference du produit
     * @return String reference
     */
    public String getReference() {
        return reference;
    }
    
    /**
     * Modifier la reference du produit
     * @param reference String reference
     */
    public void setReference(String reference) {
        this.reference = reference;
    }
    
    /**
     * Obtenir la description du produit
     * @return String descrition
     */
    public String getDescription() {
        return description;
    }
    
    /**
     * Modifier la référence du produit
     * @param description String description
     */
    public void setDescription(String description) {
        this.description = description;
    }
    
    /**
     * Obtenir le prix hors taxe du produit
     * @return 
     */
    public double getPrixht() {
        return prixht;
    }
    
    /**
     * Modifier le prix hors taxe
     * @param prixht Double prix hors taxe
     */
    public void setPrixht(double prixht) {
        this.prixht = prixht;
    }
    
    /**
     * Obtenir le type du produit
     * @return 
     */
    public String getType() {
        return type;
    }
    
    /**
     * Modifier le type du produit
     * @param type String Type
     */
    public void setType(String type) {
        this.type = type;
    }
    
    /**
     * Obetnir la quantité en stock
     * @return Integer
     */
    public int getQuantitestock() {
        return quantitestock;
    }
    
    /**
     * Modifier la quantité en stock
     * @param quantitestock Integer
     */
    public void setQuantitestock(int quantitestock) {
        this.quantitestock = quantitestock;
    }
    
    /**
     * Obtenir le taux du produit
     * @return Double taux
     */
    public double getTaux() {
        return taux;
    }
    
    /**
     * Modifier le taux du produit
     * @param taux Double 
     */
    public void setTaux(double taux) {
        this.taux = taux;
    }

    @XmlTransient
    public Collection<Commanderf> getCommanderfCollection() {
        return commanderfCollection;
    }

    public void setCommanderfCollection(Collection<Commanderf> commanderfCollection) {
        this.commanderfCollection = commanderfCollection;
    }

    @XmlTransient
    public Collection<Commanderc> getCommandercCollection() {
        return commandercCollection;
    }

    public void setCommandercCollection(Collection<Commanderc> commandercCollection) {
        this.commandercCollection = commandercCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idproduit != null ? idproduit.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Produit)) {
            return false;
        }
        Produit other = (Produit) object;
        if ((this.idproduit == null && other.idproduit != null) || (this.idproduit != null && !this.idproduit.equals(other.idproduit))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Produit{" + "idproduit=" + idproduit + ", reference=" + reference + ", description=" + description + ", prixht=" + prixht + ", type=" + type + ", quantitestock=" + quantitestock + ", taux=" + taux + '}';
    }
    
    public double getPrixTTC(){
        return prixht + (prixht * taux);
    }
    
}
