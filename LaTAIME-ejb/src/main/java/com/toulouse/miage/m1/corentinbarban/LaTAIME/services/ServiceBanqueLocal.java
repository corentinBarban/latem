/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.toulouse.miage.m1.corentinbarban.LaTAIME.services;

import com.toulouse.miage.m1.corentinbarban.lataimeshared.Position;
import exceptions.CompteInexistantException;
import exceptions.MontantInvalidException;
import java.util.List;
import javax.ejb.Local;

/**
 *Interface service banque
 */
@Local
public interface ServiceBanqueLocal {
    
    public Position consulterPosition(Integer idCompte) throws CompteInexistantException;
    
    public void crediter(Integer idCompte, Double montant) throws CompteInexistantException, MontantInvalidException;
    
    public List<Integer> getComptes();
}
