/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.toulouse.miage.m1.corentinbarban.LaTAIME.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *Classe d'un fournisseur
 */
@Entity
@Table(name = "FOURNISSEUR")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Fournisseur.findAll", query = "SELECT f FROM Fournisseur f")
    , @NamedQuery(name = "Fournisseur.findByIdfournisseur", query = "SELECT f FROM Fournisseur f WHERE f.idfournisseur = :idfournisseur")
    , @NamedQuery(name = "Fournisseur.findByNomfournisseur", query = "SELECT f FROM Fournisseur f WHERE f.nomfournisseur = :nomfournisseur")})
public class Fournisseur implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IDFOURNISSEUR")
    private Integer idfournisseur;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "NOMFOURNISSEUR")
    private String nomfournisseur;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fournisseur")
    private Collection<Commanderf> commanderfCollection;
    
    /**
     * Constructeur vide
     */ 
    public Fournisseur() {
    }
    
    /**
     * Constructeur avec l'id du fournisseur
     * @param idfournisseur Integer
     */
    public Fournisseur(Integer idfournisseur) {
        this.idfournisseur = idfournisseur;
    }
    
    /**
     * Constructeur 
     * @param idfournisseur Integer id du fournisseur
     * @param nomfournisseur String nom du fournisseur
     */
    public Fournisseur(Integer idfournisseur, String nomfournisseur) {
        this.idfournisseur = idfournisseur;
        this.nomfournisseur = nomfournisseur;
    }

    /**
     * Obtenir l'id du fournisseur
     * @return Integer id du fournisseur
     */
    public Integer getIdfournisseur() {
        return idfournisseur;
    }
    
    /**
     * Modifier l'id du fournisseur
     * @param idfournisseur Integer id du fournisseur
     */
    public void setIdfournisseur(Integer idfournisseur) {
        this.idfournisseur = idfournisseur;
    }
    
    /**
     * Obtenir le nom du fournisseur
     * @return String nom du fournisseur
     */
    public String getNomfournisseur() {
        return nomfournisseur;
    }
    /**
     * Modifier le nom du fournisseur
     * @param nomfournisseur String nom du fournisseur
     */
    public void setNomfournisseur(String nomfournisseur) {
        this.nomfournisseur = nomfournisseur;
    }
    
    /**
     * Obtenir la liste de commande fournisseur
     * @return Collection de commande
     */
    @XmlTransient
    public Collection<Commanderf> getCommanderfCollection() {
        return commanderfCollection;
    }
    
    /**
     * Modifier la liste de commande du fournisseur
     * @param commanderfCollection Collection de commande
     */
    public void setCommanderfCollection(Collection<Commanderf> commanderfCollection) {
        this.commanderfCollection = commanderfCollection;
    }
    
    /**
     * Ajouter une commande à la liste de commande
     * @param c Commande fournisseur
     */
    public void addCommandeACollection (Commanderf c){
        this.commanderfCollection.add(c);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idfournisseur != null ? idfournisseur.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Fournisseur)) {
            return false;
        }
        Fournisseur other = (Fournisseur) object;
        if ((this.idfournisseur == null && other.idfournisseur != null) || (this.idfournisseur != null && !this.idfournisseur.equals(other.idfournisseur))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.toulouse.miage.m1.corentinbarban.LaTAIME.entities.Fournisseur[ idfournisseur=" + idfournisseur + " ]";
    }
    
  
    
}
