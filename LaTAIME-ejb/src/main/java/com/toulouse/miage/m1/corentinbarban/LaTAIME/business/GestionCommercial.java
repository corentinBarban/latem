/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.toulouse.miage.m1.corentinbarban.LaTAIME.business;

import com.toulouse.miage.m1.corentinbarban.LaTAIME.entities.Fournisseur;
import com.toulouse.miage.m1.corentinbarban.LaTAIME.entities.Produit;
import exceptions.FournisseurInexistantException;
import exceptions.ProduitInexistantException;
import java.util.Collection;
import java.util.HashMap;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 * Code metier d'un commercial
 */
@Stateless
public class GestionCommercial implements GestionCommercialLocal {

    @EJB
    private GestionFournisseurLocal gestionFournisseur;

    /**
     * Effectuer une commande fournisseur
     *
     * @param produits hashmap de produit
     * @param idFournisseur id d'un fournisseur
     * @return string
     * @throws ProduitInexistantException
     * @throws FournisseurInexistantException
     */
    @Override
    public String commandeFournisseur(HashMap<Integer, Integer> produits, Integer idFournisseur) throws ProduitInexistantException, FournisseurInexistantException {
        return this.gestionFournisseur.commandeFournisseur(produits, idFournisseur);

    }

    /**
     * Lister l'ensemble des fournisseurs
     *
     * @return collection de fournisseurs
     */
    @Override
    public Collection<Fournisseur> getFournisseurs() {
        return this.gestionFournisseur.getFournisseurs();
    }

}
