/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.toulouse.miage.m1.corentinbarban.LaTAIME.business;

import com.toulouse.miage.m1.corentinbarban.LaTAIME.entities.Client;
import exceptions.ClientInexistantException;
import exceptions.MotDePasseIncorrectException;
import java.util.Collection;
import javax.ejb.Local;

/**
 * Interface de gestion client
 *
 */
@Local
public interface GestionClientLocal {

    Client authentifierClient(String identifiant, String motDePasse) throws ClientInexistantException, MotDePasseIncorrectException;

    Collection<Client> getClients();
}
