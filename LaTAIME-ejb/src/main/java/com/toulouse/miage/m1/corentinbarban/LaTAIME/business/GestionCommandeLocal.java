/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.toulouse.miage.m1.corentinbarban.LaTAIME.business;

import com.toulouse.miage.m1.corentinbarban.LaTAIME.entities.Client;
import com.toulouse.miage.m1.corentinbarban.LaTAIME.entities.Commande;
import com.toulouse.miage.m1.corentinbarban.LaTAIME.entities.Produit;
import exceptions.CommandeAcquiterException;
import exceptions.CommandeCloturerException;
import exceptions.CommandeInexistanteException;
import exceptions.QuantiteInsuffisanteException;
import exceptions.TourneeNullException;
import java.util.Collection;
import java.util.HashMap;
import javax.ejb.Local;

/**
 * Interface de gestion commande
 *
 */
@Local
public interface GestionCommandeLocal {

    public String validerPanier(HashMap<Integer, Integer> listeProduits, Integer idClient);

    public String validerCommande(Integer idCommande) throws CommandeInexistanteException, QuantiteInsuffisanteException, CommandeAcquiterException;

    public Collection<Commande> etablirTournee() throws TourneeNullException;

    public String cloturerCommande(Integer idCommande) throws CommandeInexistanteException, CommandeCloturerException;
    
    public String getStatutCommande(Integer idCommande) throws CommandeInexistanteException;
}
