/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.toulouse.miage.m1.corentinbarban.LaTAIME.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Classe d'un compte
 */
@Entity
@Table(name = "COMPTE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Compte.findAll", query = "SELECT c FROM Compte c")
    , @NamedQuery(name = "Compte.findByIdcompte", query = "SELECT c FROM Compte c WHERE c.idcompte = :idcompte")
    , @NamedQuery(name = "Compte.findBySolde", query = "SELECT c FROM Compte c WHERE c.solde = :solde")})
public class Compte implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IDCOMPTE")
    private Integer idcompte;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SOLDE")
    private double solde;
    @JoinColumn(name = "CLIENT_KEY", referencedColumnName = "IDCLIENT")
    @ManyToOne(optional = false)
    private Client clientKey;
    
    /**
     * Construcuteur vide d'un compte
     */
    public Compte() {
    }
    
    /**
     * Constructeur avec l'id d'un compte
     * @param idcompte Integer id d'un compte
     */
    public Compte(Integer idcompte) {
        this.idcompte = idcompte;
    }

    /**
     * Constructeur avec l'id et le solde du compte
     * @param idcompte Integer id d'un compte
     * @param solde Double solde du compte
     */
    public Compte(Integer idcompte, double solde) {
        this.idcompte = idcompte;
        this.solde = solde;
    }

    /**
     * Obtenir l'id d'un compte
     * 
     * @return Integer id du compte
     */
    public Integer getIdcompte() {
        return idcompte;
    }
    
    /**
     * Modifier l'id d'un compte
     * @param idcompte Integer id du compte
     */
    public void setIdcompte(Integer idcompte) {
        this.idcompte = idcompte;
    }

    /**
     * Obtenir le solde du compte
     * @return Double solde du compte
     */
    public double getSolde() {
        return solde;
    }
    
    /**
     * Modifier le solde du compte
     * @param solde Double solde du compte
     */
    public void setSolde(double solde) {
        this.solde = solde;
    }
    
    /**
     * Obtenir l'id du client
     * @return Integer id du client
     */
    public Client getClientKey() {
        return clientKey;
    }
    
    /**
     * Modifier l'id du client
     * @param clientKey Integer id du client
     */
    public void setClientKey(Client clientKey) {
        this.clientKey = clientKey;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idcompte != null ? idcompte.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Compte)) {
            return false;
        }
        Compte other = (Compte) object;
        if ((this.idcompte == null && other.idcompte != null) || (this.idcompte != null && !this.idcompte.equals(other.idcompte))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.toulouse.miage.m1.corentinbarban.LaTAIME.entities.Compte[ idcompte=" + idcompte + " ]";
    }

}
