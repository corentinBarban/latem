/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.toulouse.miage.m1.corentinbarban.LaTAIME.business;

import com.toulouse.miage.m1.corentinbarban.LaTAIME.entities.Commanderf;
import com.toulouse.miage.m1.corentinbarban.LaTAIME.entities.CommanderfPK;
import com.toulouse.miage.m1.corentinbarban.LaTAIME.entities.Fournisseur;
import com.toulouse.miage.m1.corentinbarban.LaTAIME.entities.Produit;
import com.toulouse.miage.m1.corentinbarban.LaTAIME.repositories.CommanderfFacadeLocal;
import com.toulouse.miage.m1.corentinbarban.LaTAIME.repositories.FournisseurFacadeLocal;
import com.toulouse.miage.m1.corentinbarban.LaTAIME.repositories.ProduitFacadeLocal;
import exceptions.FournisseurInexistantException;
import exceptions.ProduitInexistantException;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 * Code metier d'un fournisseur
 */
@Stateless
public class GestionFournisseur implements GestionFournisseurLocal {

    @EJB
    private CommanderfFacadeLocal commanderfFacade;

    @EJB
    private ProduitFacadeLocal produitFacade;

    @EJB
    private FournisseurFacadeLocal fournisseurFacade;

    /**
     * Réaliser une commande fournisseur
     *
     * @param produits hashmap de produits
     * @param idFournisseur identifiant d'un fournisseur
     * @return string
     * @throws ProduitInexistantException
     * @throws FournisseurInexistantException
     */
    @Override
    public String commandeFournisseur(HashMap<Integer, Integer> produits, Integer idFournisseur) throws ProduitInexistantException, FournisseurInexistantException {

        Fournisseur f = this.fournisseurFacade.find(idFournisseur);
        if (f == null) {
            throw new FournisseurInexistantException("Le fournisseur n'existe pas");
        }
        //Parcours des differents produits
        for (Map.Entry<Integer, Integer> entry : produits.entrySet()) {
            Date aujourdhui = new Date();
            Integer idProduit = entry.getKey();
            Integer nbProduits = entry.getValue();

            //Controle de l'existance du produit    
            Produit p = this.produitFacade.find(idProduit);
            if (p == null) {
                throw new ProduitInexistantException("Le produit n'existe pas");
            }
            //Création de la commande fournisseur
            CommanderfPK cfPK = new CommanderfPK(idProduit, idFournisseur, aujourdhui);
            Commanderf cf = new Commanderf(cfPK);
            cf.setNbproduit(nbProduits);
            this.commanderfFacade.create(cf);

            //Ajouter la commande aux commandes du fournisseur
            f.addCommandeACollection(cf);

            //Mise a jours de la quantité de produits en stock
            p.setQuantitestock(p.getQuantitestock() + nbProduits);

        }
        return "Votre commande fournisseur a bien été réalisé ";
    }

    /**
     * Lister l'ensemble des fournisseurs
     *
     * @return collection de fournisseur
     */
    @Override
    public Collection<Fournisseur> getFournisseurs() {
        return this.fournisseurFacade.findAll();
    }

}
