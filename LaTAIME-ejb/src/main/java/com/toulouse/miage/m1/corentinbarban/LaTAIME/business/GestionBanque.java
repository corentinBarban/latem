/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.toulouse.miage.m1.corentinbarban.LaTAIME.business;

import com.toulouse.miage.m1.corentinbarban.LaTAIME.entities.Compte;
import com.toulouse.miage.m1.corentinbarban.LaTAIME.repositories.CompteFacadeLocal;
import com.toulouse.miage.m1.corentinbarban.lataimeshared.Position;
import exceptions.ClientInexistantException;
import exceptions.CompteInexistantException;
import exceptions.MontantInvalidException;
import exceptions.SoldeInsuffisantException;
import java.util.LinkedList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Corentin
 */
@Stateless
public class GestionBanque implements GestionBanqueLocal {

    @EJB
    private CompteFacadeLocal compteFacade;

    /**
     * Permet de prelever un montant pour un compte donné
     *
     * @param idCompte id d'un compte
     * @param montant montant a prelever
     * @throws ClientInexistantException
     * @throws SoldeInsuffisantException
     */
    @Override
    public void prelever(Integer idCompte, Double montant) throws ClientInexistantException, SoldeInsuffisantException {

        Compte compte = this.compteFacade.find(idCompte);
        if (compte == null) {
            throw new ClientInexistantException("Le client n'existe pas");
        }
        double soldeCompte = compte.getSolde();

        if (soldeCompte < montant) {
            throw new SoldeInsuffisantException();
        }
        compte.setSolde(compte.getSolde() - montant);

    }

    /**
     * Consulter la position d'un compte,
     *
     * @param idCompte id d'un compte
     * @return object Position
     * @throws CompteInexistantException
     */
    @Override
    public Position consulterPosition(Integer idCompte) throws CompteInexistantException {
        //Récupération du compte et vérification des règles métiers
        final Compte c = this.compteFacade.find(idCompte);
        if (c == null) {
            throw new CompteInexistantException("Le compte n'existe pas");
        }

        return new Position(c.getSolde());
    }

    /**
     * Permet de crediter un compte avec un montant donné
     *
     * @param idCompte id d'un compte
     * @param montant montant a ajouter
     * @throws CompteInexistantException
     * @throws MontantInvalidException
     */
    @Override
    public void crediter(Integer idCompte, Double montant) throws CompteInexistantException, MontantInvalidException {
        //Récupération du compte et vérification des règles métiers
        final Compte c = this.compteFacade.find(idCompte);
        if (montant < 0.0) {
            throw new MontantInvalidException("Le montant à créditer ne peut pas être négatif.");
        }
        if (c == null) {
            throw new CompteInexistantException();
        }

        //Opération métier
        c.setSolde(c.getSolde() + montant);
        this.compteFacade.edit(c);
    }

    /**
     * Lister l'ensemble des comptes
     *
     * @return list de comtpe
     */
    @Override
    public List<Integer> getComptes() {
        List<Compte> comptes = this.compteFacade.findAll();
        List<Integer> comptesInt = new LinkedList<>();
        for (Compte c : comptes) {
            comptesInt.add(c.getIdcompte());
        }
        return comptesInt;
    }

}
