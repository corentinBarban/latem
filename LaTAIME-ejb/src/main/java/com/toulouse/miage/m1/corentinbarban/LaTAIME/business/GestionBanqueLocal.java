/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.toulouse.miage.m1.corentinbarban.LaTAIME.business;

import com.toulouse.miage.m1.corentinbarban.LaTAIME.entities.Compte;
import com.toulouse.miage.m1.corentinbarban.lataimeshared.Position;
import exceptions.ClientInexistantException;
import exceptions.CompteInexistantException;
import exceptions.MontantInvalidException;
import exceptions.SoldeInsuffisantException;
import java.util.List;
import javax.ejb.Local;

/**
 * Interface de gestion banque
 *
 */
@Local
public interface GestionBanqueLocal {

    public void prelever(Integer idCompte, Double montant) throws ClientInexistantException, SoldeInsuffisantException;

    public Position consulterPosition(Integer idCompte) throws CompteInexistantException;

    public void crediter(Integer idCompte, Double montant) throws CompteInexistantException, MontantInvalidException;

    public List<Integer> getComptes();
}
