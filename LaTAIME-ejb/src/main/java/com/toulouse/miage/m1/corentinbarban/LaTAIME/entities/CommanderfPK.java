/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.toulouse.miage.m1.corentinbarban.LaTAIME.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Corentin
 */
@Embeddable
public class CommanderfPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "PRODUIT_KEY")
    private int produitKey;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FOURNISSEUR_KEY")
    private int fournisseurKey;
    @Basic(optional = false)
    @NotNull
    @Column(name = "DATECOMMANDEF")
    @Temporal(TemporalType.DATE)
    private Date datecommandef;
    
    /**
     * Constructeur vide 
     */
    public CommanderfPK() {
    }
    
    /**
     * Constructeur avec les clé primaire
     * @param produitKey id d'un produit
     * @param fournisseurKey id d'un fournisseur
     * @param datecommandef date de la commande fournisseur
     */
    public CommanderfPK(int produitKey, int fournisseurKey, Date datecommandef) {
        this.produitKey = produitKey;
        this.fournisseurKey = fournisseurKey;
        this.datecommandef = datecommandef;
    }
    
    /**
     * Obtenir l'id du produit
     * @return Integer
     */
    public int getProduitKey() {
        return produitKey;
    }

    /**
     * Modifier l'id d'un produit
     * @param produitKey Integer
     */
    public void setProduitKey(int produitKey) {
        this.produitKey = produitKey;
    }

    /**
     * Obtenir l'id du fournisseur
     * @return Integer
     */
    public int getFournisseurKey() {
        return fournisseurKey;
    }

    /**
     * Modifier l'id du fournisseur
     * @param fournisseurKey 
     */
    public void setFournisseurKey(int fournisseurKey) {
        this.fournisseurKey = fournisseurKey;
    }

    /**
     * Obtenir la date de commande
     * @return Date de commande
     */
    public Date getDatecommandef() {
        return datecommandef;
    }
    
    /**
     * Changer la date de la commande
     * @param datecommandef date
     */
    public void setDatecommandef(Date datecommandef) {
        this.datecommandef = datecommandef;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) produitKey;
        hash += (int) fournisseurKey;
        hash += (datecommandef != null ? datecommandef.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CommanderfPK)) {
            return false;
        }
        CommanderfPK other = (CommanderfPK) object;
        if (this.produitKey != other.produitKey) {
            return false;
        }
        if (this.fournisseurKey != other.fournisseurKey) {
            return false;
        }
        if ((this.datecommandef == null && other.datecommandef != null) || (this.datecommandef != null && !this.datecommandef.equals(other.datecommandef))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.toulouse.miage.m1.corentinbarban.LaTAIME.entities.CommanderfPK[ produitKey=" + produitKey + ", fournisseurKey=" + fournisseurKey + ", datecommandef=" + datecommandef + " ]";
    }
    
}
