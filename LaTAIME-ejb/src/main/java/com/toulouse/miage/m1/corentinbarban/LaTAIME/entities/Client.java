/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.toulouse.miage.m1.corentinbarban.LaTAIME.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Entité Client
 * 
 */
@Entity
@Table(name = "CLIENT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Client.findAll", query = "SELECT c FROM Client c")
    , @NamedQuery(name = "Client.findByIdclient", query = "SELECT c FROM Client c WHERE c.idclient = :idclient")
    , @NamedQuery(name = "Client.findByNom", query = "SELECT c FROM Client c WHERE c.nom = :nom")
    , @NamedQuery(name = "Client.findByPrenom", query = "SELECT c FROM Client c WHERE c.prenom = :prenom")
    , @NamedQuery(name = "Client.findByIdentifiant", query = "SELECT c FROM Client c WHERE c.identifiant = :identifiant")
    , @NamedQuery(name = "Client.findByPassword", query = "SELECT c FROM Client c WHERE c.password = :password")})
public class Client implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IDCLIENT")
    private Integer idclient;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "NOM")
    private String nom;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "PRENOM")
    private String prenom;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "IDENTIFIANT")
    private String identifiant;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "PASSWORD")
    private String password;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "clientKey")
    private Collection<Compte> compteCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "client")
    private Collection<Commanderc> commandercCollection;
    
    /**
     * Constructeur vide
     */
    public Client() {
    }
    
    /**
     * Constructeur d'un client 
     * @param idclient id du client
     */
    public Client(Integer idclient) {
        this.idclient = idclient;
    }
    
    /**
     * Constructeur d'un client avec tous les attributs
     * @param idclient id du client
     * @param nom nom du client
     * @param prenom prenom d'un client
     * @param identifiant identifiant du client
     * @param password password d'un client
     */
    public Client(Integer idclient, String nom, String prenom, String identifiant, String password) {
        this.idclient = idclient;
        this.nom = nom;
        this.prenom = prenom;
        this.identifiant = identifiant;
        this.password = password;
    }
    
    /**
     * Obtenir l'id d'un client
     * @return integer
     */
    public Integer getIdclient() {
        return idclient;
    }
    
    /**
     * Modifier d'un client
     * 
     * @param idclient id du client
     */
    public void setIdclient(Integer idclient) {
        this.idclient = idclient;
    }
    
    /**
     * Obtenir le nom d'un client
     * @return string
     */
    public String getNom() {
        return nom;
    }
    
    /**
     * Modifier le nom d'un client
     * @param nom nom du client
     */
    public void setNom(String nom) {
        this.nom = nom;
    }
    
    /**
     * Obtenir le prenom d'un client
     * @return string
     */
    public String getPrenom() {
        return prenom;
    }
    
    /**
     * Modifier le prenom
     * @param prenom prenom du client
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
    
    /**
     * Obtenir l'identifiant d'un client
     * @return string
     */
    public String getIdentifiant() {
        return identifiant;
    }
    
    /**
     * Modifier l'identifiant d'un client
     * @param identifiant string 
     */
    public void setIdentifiant(String identifiant) {
        this.identifiant = identifiant;
    }
    
    /**
     * Obtenir le password d'un client
     * @return string 
     */
    public String getPassword() {
        return password;
    }
    
    /**
     * Modifier le password
     * @param password password d'un client
     */
    public void setPassword(String password) {
        this.password = password;
    }
    
    /**
     * Obtenir la liste de compte
     * @return collection
     */
    @XmlTransient
    public Collection<Compte> getCompteCollection() {
        return compteCollection;
    }
    
    /**
     * Modifier la liste de compte
     * @param compteCollection collection de compte
     */
    public void setCompteCollection(Collection<Compte> compteCollection) {
        this.compteCollection = compteCollection;
    }
    
    /**
     * Obtenir la liste de commande d'un client
     * @return collection
     */
    @XmlTransient
    public Collection<Commanderc> getCommandercCollection() {
        return commandercCollection;
    }
    
    /**
     * Modifier la liste de commande
     * @param commandercCollection  collection de commande
     */
    public void setCommandercCollection(Collection<Commanderc> commandercCollection) {
        this.commandercCollection = commandercCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idclient != null ? idclient.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Client)) {
            return false;
        }
        Client other = (Client) object;
        if ((this.idclient == null && other.idclient != null) || (this.idclient != null && !this.idclient.equals(other.idclient))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.toulouse.miage.m1.corentinbarban.LaTAIME.entities.Client[ idclient=" + idclient + " ]";
    }
    
}
