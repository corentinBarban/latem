/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.toulouse.miage.m1.corentinbarban.LaTAIME.repositories;

import com.toulouse.miage.m1.corentinbarban.LaTAIME.entities.Commanderf;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *Facade commande fournisseur
 */
@Stateless
public class CommanderfFacade extends AbstractFacade<Commanderf> implements CommanderfFacadeLocal {

    @EJB
    private FournisseurFacadeLocal fournisseurFacade;

    @PersistenceContext(unitName = "LaTAIMEPersistanceUnit")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CommanderfFacade() {
        super(Commanderf.class);
    }
    
}
