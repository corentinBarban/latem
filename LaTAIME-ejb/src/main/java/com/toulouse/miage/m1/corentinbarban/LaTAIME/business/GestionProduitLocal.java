/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.toulouse.miage.m1.corentinbarban.LaTAIME.business;

import com.toulouse.miage.m1.corentinbarban.LaTAIME.entities.Produit;
import java.util.Collection;
import javax.ejb.Local;

/**
 * Interface gestion de produit
 * 
 */
@Local
public interface GestionProduitLocal {
    public Collection<Produit> getProduits();
}
