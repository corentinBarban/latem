/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.toulouse.miage.m1.corentinbarban.LaTAIME.business;

import com.toulouse.miage.m1.corentinbarban.LaTAIME.entities.Client;
import com.toulouse.miage.m1.corentinbarban.LaTAIME.entities.Commande;
import com.toulouse.miage.m1.corentinbarban.LaTAIME.entities.Commanderc;
import com.toulouse.miage.m1.corentinbarban.LaTAIME.entities.CommandercPK;
import com.toulouse.miage.m1.corentinbarban.LaTAIME.entities.Compte;
import com.toulouse.miage.m1.corentinbarban.LaTAIME.entities.Produit;
import com.toulouse.miage.m1.corentinbarban.LaTAIME.repositories.ClientFacadeLocal;
import com.toulouse.miage.m1.corentinbarban.LaTAIME.repositories.CommandeFacadeLocal;
import com.toulouse.miage.m1.corentinbarban.LaTAIME.repositories.CommandercFacadeLocal;
import com.toulouse.miage.m1.corentinbarban.LaTAIME.repositories.ProduitFacadeLocal;
import exceptions.ClientInexistantException;
import exceptions.CommandeAcquiterException;
import exceptions.CommandeCloturerException;
import exceptions.CommandeInexistanteException;
import exceptions.QuantiteInsuffisanteException;
import exceptions.SoldeInsuffisantException;
import exceptions.TourneeNullException;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 * Code metier d'une commande
 *
 */
@Stateless
public class GestionCommande implements GestionCommandeLocal {

    @EJB
    private ProduitFacadeLocal produitFacade;

    @EJB
    private CommandeFacadeLocal commandeFacade;

    @EJB
    private CommandercFacadeLocal commandercFacade;

    @EJB
    private ClientFacadeLocal clientFacade;

    @EJB
    private GestionBanqueLocal gestionBanque;

    /**
     * Fonction de validation d'un panier
     *
     * @param listeProduits sous la forme d'une HashMap idProduit,Quantite
     * @param idClient id d'un client
     * @return String Id de la commande
     */
    @Override
    public String validerPanier(HashMap<Integer, Integer> listeProduits, Integer idClient) {

        Date aujourdhui = new Date();
        Commande commande = new Commande(aujourdhui, "En cours");
        commande = this.commandeFacade.create(commande);

        for (Map.Entry<Integer, Integer> entry : listeProduits.entrySet()) {
            Integer idProduit = entry.getKey();
            Integer nbProduits = entry.getValue();
            Integer idCommande = commande.getIdcommande();
            //Creation de la clé étrangére
            CommandercPK commandeCPK = new CommandercPK(idProduit, idClient, idCommande);
            //Creation de la commande client (classe d'association)
            Commanderc commandeClient = new Commanderc(commandeCPK);

            commandeClient.setNbproduit(nbProduits);
            this.commandercFacade.create(commandeClient);

        }
        return commande.getIdcommande().toString();
    }

    /**
     * Traiter une commande afin de changer son statut à en cours et Prelever le
     * client
     *
     * @param idCommande id de la ommande
     * @return String Id de la commande
     * @throws CommandeInexistanteException
     * @throws QuantiteInsuffisanteException
     * @throws CommandeAcquiterException
     */
    @Override
    public String validerCommande(Integer idCommande) throws CommandeInexistanteException, QuantiteInsuffisanteException, CommandeAcquiterException {

        Commande commandeClient = this.commandeFacade.find(idCommande);
        double prixTTC = 0;
        if (commandeClient == null) {

            throw new CommandeInexistanteException("La commande n'existe pas");

        } else if (!commandeClient.getStatut().equals("En cours")) {

            throw new CommandeAcquiterException("La commande à déja été traitée");

        } else {
            Collection<Commanderc> listeProduitQuantite = commandeClient.getCommandercCollection();
            for (Commanderc commanderc : listeProduitQuantite) {

                int nbProduitCommande = commanderc.getNbproduit();
                int nbProduitStock = this.produitFacade.find(commanderc.getProduit().getIdproduit()).getQuantitestock();

                if (nbProduitStock < nbProduitCommande) {
                    throw new QuantiteInsuffisanteException("Le stock n'est pas suffisant pour le produit " + commanderc.getProduit().getReference());
                }

                // Calcule prix TTC
                prixTTC += this.produitFacade.find(commanderc.getProduit().getIdproduit()).getPrixTTC()*nbProduitCommande;

            }
            commandeClient.setStatut("Acquiter");
            Client client = listeProduitQuantite.iterator().next().getClient();
            Compte compteClient = client.getCompteCollection().iterator().next();

            try {

                this.gestionBanque.prelever(compteClient.getIdcompte(), prixTTC);

            } catch (ClientInexistantException | SoldeInsuffisantException ex) {

                Logger.getLogger(GestionCommande.class.getName()).log(Level.SEVERE, null, ex);

            }

        }
        return commandeClient.getIdcommande().toString();

    }

    /**
     * Etablir la liste des commandes possedant le statut Acquiter
     *
     * @return Collection de commande
     * @throws TourneeNullException
     */
    @Override
    public Collection<Commande> etablirTournee() throws TourneeNullException {
        // Récuperer toutes les commandes clients
        List<Commande> s = this.commandeFacade.findAll();
        Collection<Commande> res = new HashSet();
        // Parcourir les commandes et ne garder que celles qui sont acquite
        for (Commande c : s) {
            if (c.getStatut().equals("Acquiter")) {
                res.add(c);
            }
        }

        if (res.isEmpty()) {
            throw new TourneeNullException("Il n'y a aucune livraison à faire.");
        }
        return res;
    }

    /**
     * Cloturer une commande en changeant son statut et MAJ des stocks
     *
     * @param idCommande
     * @return String
     * @throws CommandeInexistanteException
     * @throws CommandeCloturerException
     */
    @Override
    public String cloturerCommande(Integer idCommande) throws CommandeInexistanteException, CommandeCloturerException {
        Commande commandeClient = this.commandeFacade.find(idCommande);
        if (commandeClient == null) {

            throw new CommandeInexistanteException("La commande n'existe pas");

        } else if (commandeClient.getStatut().equals("Cloturée")) {

            throw new CommandeCloturerException("La commande à déja été cloturée");

        } else {

            Collection<Commanderc> listeProduitQuantite = commandeClient.getCommandercCollection();
            for (Commanderc commanderc : listeProduitQuantite) {
                int nbProduitCommande = commanderc.getNbproduit();
                commanderc.getProduit().setQuantitestock(commanderc.getProduit().getQuantitestock() - nbProduitCommande);
            }

            commandeClient.setStatut("Cloturée");
        }
        return "La commande est cloturée";
    }

    @Override
    public String getStatutCommande(Integer idCommande) throws CommandeInexistanteException {
        Commande commandeClient = this.commandeFacade.find(idCommande);
        if (commandeClient == null) {

            throw new CommandeInexistanteException("La commande n'existe pas");

        }
        String statut = commandeClient.getStatut();
        return statut;
    }

}
