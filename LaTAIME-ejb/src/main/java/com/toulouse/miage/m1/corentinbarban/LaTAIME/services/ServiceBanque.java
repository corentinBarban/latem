/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.toulouse.miage.m1.corentinbarban.LaTAIME.services;

import com.toulouse.miage.m1.corentinbarban.LaTAIME.business.GestionBanqueLocal;
import com.toulouse.miage.m1.corentinbarban.lataimeshared.Position;
import exceptions.CompteInexistantException;
import exceptions.MontantInvalidException;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 * Classe service banque
 */
@Stateless
public class ServiceBanque implements ServiceBanqueRemote, ServiceBanqueLocal {

    @EJB
    private GestionBanqueLocal gestionBanque;

    
    /**
     * Consulter la position d'un compte
     * @param idCompte Integer id du compte
     * @return Position
     * @throws CompteInexistantException 
     */
    @Override
    public Position consulterPosition(Integer idCompte) throws CompteInexistantException {
       //Récupération du compte et vérification des règles métiers
        return this.gestionBanque.consulterPosition(idCompte);
    }
    
    /**
     * Crediter un compte
     * 
     * @param idCompte Integer id du compte
     * @param montant Double montant
     * @throws CompteInexistantException 
     * @throws MontantInvalidException 
     */
    @Override
    public void crediter(Integer idCompte, Double montant) throws CompteInexistantException, MontantInvalidException {
        this.gestionBanque.crediter(idCompte, montant);
    }

    /**
     * Obtenir la liste des comptes
     * @return list de comptes
     */
    @Override
    public List<Integer> getComptes() {
        return this.gestionBanque.getComptes();
    }
    




}
