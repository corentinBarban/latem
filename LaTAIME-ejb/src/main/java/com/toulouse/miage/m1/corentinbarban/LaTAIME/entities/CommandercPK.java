/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.toulouse.miage.m1.corentinbarban.LaTAIME.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 * Classe  compostion de la clé primaire
 */
@Embeddable
public class CommandercPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "PRODUIT_KEY")
    private int produitKey;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CLIENT_KEY")
    private int clientKey;
    @Basic(optional = false)
    @NotNull
    @Column(name = "COMMANDE_KEY")
    private int commandeKey;

    /**
     * Constructeur vide
     */
    public CommandercPK() {
    }
    
    /**
     * Constructeur de la clé primaire
     * @param produitKey id d'un produit
     * @param clientKey id d'un client
     * @param commandeKey id d'une commande
     */
    public CommandercPK(int produitKey, int clientKey, int commandeKey) {
        this.produitKey = produitKey;
        this.clientKey = clientKey;
        this.commandeKey = commandeKey;
    }
    
    /**
     * Obtenir l'id de produit
     * @return produit
     */
    public int getProduitKey() {
        return produitKey;
    }
    
    /**
     * Modifier l'id du produit
     * @param produitKey id d'un produit
     */
    public void setProduitKey(int produitKey) {
        this.produitKey = produitKey;
    }
    /**
     * Obtenir l'id du client
     * @return id d'un client
     */
    public int getClientKey() {
        return clientKey;
    }
    
     /**
     * Modifier l'id du client
     * @param clientKey id d'un client
     */
    public void setClientKey(int clientKey) {
        this.clientKey = clientKey;
    }
    
     /**
     * Obtenir l'id de la commande
     * @return  id d'une commande
     */
    public int getCommandeKey() {
        return commandeKey;
    }
    
    /**
     * Modifier l'ide de la commande
     * @param commandeKey id d'une commande
     */
    public void setCommandeKey(int commandeKey) {
        this.commandeKey = commandeKey;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) produitKey;
        hash += (int) clientKey;
        hash += (int) commandeKey;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CommandercPK)) {
            return false;
        }
        CommandercPK other = (CommandercPK) object;
        if (this.produitKey != other.produitKey) {
            return false;
        }
        if (this.clientKey != other.clientKey) {
            return false;
        }
        if (this.commandeKey != other.commandeKey) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.toulouse.miage.m1.corentinbarban.LaTAIME.entities.CommandercPK[ produitKey=" + produitKey + ", clientKey=" + clientKey + ", commandeKey=" + commandeKey + " ]";
    }

}
