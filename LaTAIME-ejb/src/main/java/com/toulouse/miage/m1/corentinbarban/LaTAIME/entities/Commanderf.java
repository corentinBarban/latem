/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.toulouse.miage.m1.corentinbarban.LaTAIME.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Classe de commande fournisseur
 */
@Entity
@Table(name = "COMMANDERF")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Commanderf.findAll", query = "SELECT c FROM Commanderf c")
    , @NamedQuery(name = "Commanderf.findByProduitKey", query = "SELECT c FROM Commanderf c WHERE c.commanderfPK.produitKey = :produitKey")
    , @NamedQuery(name = "Commanderf.findByFournisseurKey", query = "SELECT c FROM Commanderf c WHERE c.commanderfPK.fournisseurKey = :fournisseurKey")
    , @NamedQuery(name = "Commanderf.findByNbproduit", query = "SELECT c FROM Commanderf c WHERE c.nbproduit = :nbproduit")
    , @NamedQuery(name = "Commanderf.findByDatecommandef", query = "SELECT c FROM Commanderf c WHERE c.commanderfPK.datecommandef = :datecommandef")})
public class Commanderf implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CommanderfPK commanderfPK;
    @Column(name = "NBPRODUIT")
    private Integer nbproduit;
    @JoinColumn(name = "FOURNISSEUR_KEY", referencedColumnName = "IDFOURNISSEUR", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Fournisseur fournisseur;
    @JoinColumn(name = "PRODUIT_KEY", referencedColumnName = "IDPRODUIT", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Produit produit;

    /**
     * Constructeur vide d'une commande fournisseur
     */
    public Commanderf() {
    }

    /**
     * Constructeur d'une commande fournisseur avec ca clé primaire
     *
     * @param commanderfPK object
     */
    public Commanderf(CommanderfPK commanderfPK) {
        this.commanderfPK = commanderfPK;
    }

    /**
     * Construcuteur d'une commande fournisseur avec les attributs de la clé
     * primaire
     *
     * @param produitKey id d'un produit
     * @param fournisseurKey id d'un fournisseur
     * @param datecommandef date d'une commande
     */
    public Commanderf(int produitKey, int fournisseurKey, Date datecommandef) {
        this.commanderfPK = new CommanderfPK(produitKey, fournisseurKey, datecommandef);
    }

    /**
     * Obtenir la clé primaire
     *
     * @return object
     */
    public CommanderfPK getCommanderfPK() {
        return commanderfPK;
    }

    /**
     * Modifier la clé primaire
     *
     * @param commanderfPK Object
     */
    public void setCommanderfPK(CommanderfPK commanderfPK) {
        this.commanderfPK = commanderfPK;
    }

    /**
     * Obtenir le nombre de produits commandés
     *
     * @return Integer nombre de produits
     */
    public Integer getNbproduit() {
        return nbproduit;
    }

    /**
     * Modifier le nombre de produits commandés
     *
     * @param nbproduit Integer nombre de produit
     */
    public void setNbproduit(Integer nbproduit) {
        this.nbproduit = nbproduit;
    }

    /**
     * Obtenir le fournisseur
     *
     * @return object fournisseur
     */
    public Fournisseur getFournisseur() {
        return fournisseur;
    }

    /**
     * Modifier le fournisseur
     *
     * @param fournisseur
     */
    public void setFournisseur(Fournisseur fournisseur) {
        this.fournisseur = fournisseur;
    }

    /**
     * Obtenir le produit
     *
     * @return Object produit
     */
    public Produit getProduit() {
        return produit;
    }

    /**
     * Modifier le produit
     *
     * @param produit object Produit
     */
    public void setProduit(Produit produit) {
        this.produit = produit;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (commanderfPK != null ? commanderfPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Commanderf)) {
            return false;
        }
        Commanderf other = (Commanderf) object;
        if ((this.commanderfPK == null && other.commanderfPK != null) || (this.commanderfPK != null && !this.commanderfPK.equals(other.commanderfPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.toulouse.miage.m1.corentinbarban.LaTAIME.entities.Commanderf[ commanderfPK=" + commanderfPK + " ]";
    }

}
