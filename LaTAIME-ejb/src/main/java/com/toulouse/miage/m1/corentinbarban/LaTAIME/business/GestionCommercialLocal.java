/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.toulouse.miage.m1.corentinbarban.LaTAIME.business;

import com.toulouse.miage.m1.corentinbarban.LaTAIME.entities.Fournisseur;
import com.toulouse.miage.m1.corentinbarban.LaTAIME.entities.Produit;
import exceptions.FournisseurInexistantException;
import exceptions.ProduitInexistantException;
import java.util.Collection;
import java.util.HashMap;
import javax.ejb.Local;

/**
 * Interface gestion commerciale
 *
 */
@Local
public interface GestionCommercialLocal {

    public String commandeFournisseur(HashMap<Integer, Integer> produits, Integer idFournisseur) throws ProduitInexistantException, FournisseurInexistantException;

    public Collection<Fournisseur> getFournisseurs();
}
