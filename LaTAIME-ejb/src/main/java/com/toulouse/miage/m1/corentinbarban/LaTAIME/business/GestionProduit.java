/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.toulouse.miage.m1.corentinbarban.LaTAIME.business;

import com.toulouse.miage.m1.corentinbarban.LaTAIME.entities.Produit;
import com.toulouse.miage.m1.corentinbarban.LaTAIME.repositories.ProduitFacadeLocal;
import java.util.Collection;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 * Code metier d'un produit
 * 
 */
@Stateless
public class GestionProduit implements GestionProduitLocal {

    @EJB
    private ProduitFacadeLocal produitFacade;
    
    /**
     * Lister l'ensemble des produits
     * @return collection de produits
     */
    @Override
    public Collection<Produit> getProduits() {
         return this.produitFacade.findAll();
    }

   
}
