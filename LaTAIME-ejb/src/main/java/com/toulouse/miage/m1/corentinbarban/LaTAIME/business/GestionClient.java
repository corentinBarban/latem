/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.toulouse.miage.m1.corentinbarban.LaTAIME.business;

import com.toulouse.miage.m1.corentinbarban.LaTAIME.entities.Client;
import com.toulouse.miage.m1.corentinbarban.LaTAIME.repositories.ClientFacadeLocal;
import exceptions.ClientInexistantException;
import exceptions.MotDePasseIncorrectException;
import java.util.Collection;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Corentin
 */
@Stateless
public class GestionClient implements GestionClientLocal {

    @EJB
    private ClientFacadeLocal clientFacade;

    /**
     * Systéme d'authentification d'un client
     *
     * @param identifiant identifiant du compte client
     * @param motDePasse mdp du compte client
     * @return object Client
     * @throws ClientInexistantException
     * @throws MotDePasseIncorrectException
     */
    @Override
    public Client authentifierClient(String identifiant, String motDePasse) throws ClientInexistantException, MotDePasseIncorrectException {
        //Récupération du client à partir de son login     
        Client client = this.clientFacade.findByIdentifiant(identifiant);

        if (client == null) {
            throw new ClientInexistantException("Le client n'existe pas");
        }

        if (!client.getPassword().equals(motDePasse)) {
            throw new MotDePasseIncorrectException("Le mot de passe est incorrect");
        }

        return client;
    }

    /**
     * Lister l'ensemble des clients présents dans le systeme
     *
     * @return collection de client
     */
    @Override
    public Collection<Client> getClients() {
        return this.clientFacade.findAll();
    }

}
