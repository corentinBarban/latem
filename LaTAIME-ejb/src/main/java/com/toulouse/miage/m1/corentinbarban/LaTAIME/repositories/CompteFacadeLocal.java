/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.toulouse.miage.m1.corentinbarban.LaTAIME.repositories;

import com.toulouse.miage.m1.corentinbarban.LaTAIME.entities.Compte;
import com.toulouse.miage.m1.corentinbarban.lataimeshared.Position;
import exceptions.CompteInexistantException;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Corentin
 */
@Local
public interface CompteFacadeLocal {

    Compte create(Compte compte);

    void edit(Compte compte);

    void remove(Compte compte);

    Compte find(Object id);

    List<Compte> findAll();

    List<Compte> findRange(int[] range);

    int count();
    
}
