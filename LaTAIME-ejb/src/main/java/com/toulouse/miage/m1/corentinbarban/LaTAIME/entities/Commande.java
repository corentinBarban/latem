/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.toulouse.miage.m1.corentinbarban.LaTAIME.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Entité commande
 */
@Entity
@Table(name = "COMMANDE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Commande.findAll", query = "SELECT c FROM Commande c")
    , @NamedQuery(name = "Commande.findByIdcommande", query = "SELECT c FROM Commande c WHERE c.idcommande = :idcommande")
    , @NamedQuery(name = "Commande.findByDatecommande", query = "SELECT c FROM Commande c WHERE c.datecommande = :datecommande")
    , @NamedQuery(name = "Commande.findByStatut", query = "SELECT c FROM Commande c WHERE c.statut = :statut")})
public class Commande implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IDCOMMANDE")
    private Integer idcommande;
    @Basic(optional = false)
    @NotNull
    @Column(name = "DATECOMMANDE")
    @Temporal(TemporalType.DATE)
    private Date datecommande;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "STATUT")
    private String statut;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "commande")
    private Collection<Commanderc> commandercCollection;
    
    /**
     * constructeur vide d'une commande
     */
    public Commande() {
    }
    
    /**
     * Constructeur d'une commande
     * @param idcommande id d'une commande
     */
    public Commande(Integer idcommande) {
        this.idcommande = idcommande;
    }
    
    /**
     * Constructeur d'une commande avec tous les attributs
     * @param idcommande id de la commande
     * @param datecommande date de la commande
     * @param statut statut de la commande
     */
    public Commande(Integer idcommande, Date datecommande, String statut) {
        this.idcommande = idcommande;
        this.datecommande = datecommande;
        this.statut = statut;
    }
        
    /**
     * constructeur d'une commande avec la date et le statut
     * @param datecommande date de la commande
     * @param statut statut de la commande
     */
    public Commande(Date datecommande, String statut) {
        this.datecommande = datecommande;
        this.statut = statut;
    }
    
    /**
     * Obtenir l'id de la commande
     * @return integer id de la commande
     */
    public Integer getIdcommande() {
        return idcommande;
    }
    
    /**
     * Modifier l'id de la commande
     * 
     * @param idcommande integer id de la commande
     */
    public void setIdcommande(Integer idcommande) {
        this.idcommande = idcommande;
    }
    
    /**
     * Obtenir la date de la commande
     * 
     * @return date de la commande
     */
    public Date getDatecommande() {
        return datecommande;
    }
    
    /**
     * Modifier la date de la commande
     * 
     * @param datecommande date de la commande
     */
    public void setDatecommande(Date datecommande) {
        this.datecommande = datecommande;
    }
    
    /**
     * Obtenir le status de la commande 
     * 
     * @return string
     */
    public String getStatut() {
        return statut;
    }
    
    /**
     * Modifier le status de la commande
     * @param statut statut de la commande
     */
    public void setStatut(String statut) {
        this.statut = statut;
    }
    
    /**
     * Obtenir la liste des commandes
     * 
     * @return collection
     */
    @XmlTransient
    public Collection<Commanderc> getCommandercCollection() {
        return commandercCollection;
    }
    
    /**
     * Modifier la liste de commande client
     * @param commandercCollection collection d'une commande
     */
    public void setCommandercCollection(Collection<Commanderc> commandercCollection) {
        this.commandercCollection = commandercCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idcommande != null ? idcommande.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Commande)) {
            return false;
        }
        Commande other = (Commande) object;
        if ((this.idcommande == null && other.idcommande != null) || (this.idcommande != null && !this.idcommande.equals(other.idcommande))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Commande{" + "idcommande=" + idcommande + ", datecommande=" + datecommande + ", statut=" + statut + '}';
    }
    
}
