/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.toulouse.miage.m1.corentinbarban.LaTAIME.repositories;

import com.toulouse.miage.m1.corentinbarban.LaTAIME.entities.Commanderf;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Corentin
 */
@Local
public interface CommanderfFacadeLocal {

    Commanderf create(Commanderf commanderf);

    void edit(Commanderf commanderf);

    void remove(Commanderf commanderf);

    Commanderf find(Object id);

    List<Commanderf> findAll();

    List<Commanderf> findRange(int[] range);

    int count();
    
}
