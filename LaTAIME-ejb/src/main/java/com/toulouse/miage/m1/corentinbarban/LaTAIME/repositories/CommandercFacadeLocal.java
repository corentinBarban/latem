/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.toulouse.miage.m1.corentinbarban.LaTAIME.repositories;

import com.toulouse.miage.m1.corentinbarban.LaTAIME.entities.Commanderc;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Corentin
 */
@Local
public interface CommandercFacadeLocal {

    Commanderc create(Commanderc commanderc);

    void edit(Commanderc commanderc);

    void remove(Commanderc commanderc);

    Commanderc find(Object id);

    List<Commanderc> findAll();

    List<Commanderc> findRange(int[] range);

    int count();
    
}
