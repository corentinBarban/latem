/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.toulouse.miage.m1.corentinbarban.LaTAIME.business;

import com.toulouse.miage.m1.corentinbarban.LaTAIME.entities.Commande;
import exceptions.TourneeNullException;
import java.util.Collection;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * Code metier pour les livraisons
 *
 */
@Stateless
public class GestionLivraison implements GestionLivraisonLocal {

    @EJB
    private GestionCommandeLocal gestionCommande;

    /**
     * Etablir la liste de commande à livrer
     *
     * @return collection de commande
     * @throws TourneeNullException
     */
    @Override
    public Collection<Commande> etablirTournee() throws TourneeNullException {
        return this.gestionCommande.etablirTournee();
    }

}
