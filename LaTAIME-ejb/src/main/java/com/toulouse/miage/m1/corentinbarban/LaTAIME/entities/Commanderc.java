/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.toulouse.miage.m1.corentinbarban.LaTAIME.entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * Entité classe d'association d'une commande client
 */
@Entity
@Table(name = "COMMANDERC")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Commanderc.findAll", query = "SELECT c FROM Commanderc c")
    , @NamedQuery(name = "Commanderc.findByNbproduit", query = "SELECT c FROM Commanderc c WHERE c.nbproduit = :nbproduit")
    , @NamedQuery(name = "Commanderc.findByProduitKey", query = "SELECT c FROM Commanderc c WHERE c.commandercPK.produitKey = :produitKey")
    , @NamedQuery(name = "Commanderc.findByClientKey", query = "SELECT c FROM Commanderc c WHERE c.commandercPK.clientKey = :clientKey")
    , @NamedQuery(name = "Commanderc.findByCommandeKey", query = "SELECT c FROM Commanderc c WHERE c.commandercPK.commandeKey = :commandeKey")})
public class Commanderc implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CommandercPK commandercPK;
    @Column(name = "NBPRODUIT")
    private Integer nbproduit;
    @JoinColumn(name = "CLIENT_KEY", referencedColumnName = "IDCLIENT", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Client client;
    @JoinColumn(name = "COMMANDE_KEY", referencedColumnName = "IDCOMMANDE", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Commande commande;
    @JoinColumn(name = "PRODUIT_KEY", referencedColumnName = "IDPRODUIT", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Produit produit;
    
    /**
     * Construceur vide d'une commande client
     */
    public Commanderc() {
    }
    
    /**
     * Constructeur avec la clé primaire
     * @param commandercPK object commandercPK
     */
    public Commanderc(CommandercPK commandercPK) {
        this.commandercPK = commandercPK;
    }
    
    /**
     * Constructeur avec les attributs de la clé primaire
     * @param produitKey id d'un produit
     * @param clientKey id d'un client
     * @param commande id d'une commande
     */
    public Commanderc(int produitKey, int clientKey, Commande commande) {
        this.commandercPK = new CommandercPK(produitKey, clientKey, commande.getIdcommande());
    }
    
    /**
     * Obtenir la clé primaire
     * @return CommanddercPK
     */
    public CommandercPK getCommandercPK() {
        return commandercPK;
    }
    
    /**
     * Modifier la clé primaire
     * @param commandercPK Object CommandercPK
     */
    public void setCommandercPK(CommandercPK commandercPK) {
        this.commandercPK = commandercPK;
    }
    
    /**
     * Obtenir le nombre de produits
     * @return Integer
     */
    public Integer getNbproduit() {
        return nbproduit;
    }
    
    /**
     * Modifier le nombre de produits
     * @param nbproduit Integer nombre de produit
     */
    public void setNbproduit(Integer nbproduit) {
        this.nbproduit = nbproduit;
    }
    
    /**
     * Obtenir le client
     * @return Client
     */
    public Client getClient() {
        return client;
    }
    
    /**
     * Modifier le client
     * @param client Object Client
     */
    public void setClient(Client client) {
        this.client = client;
    }

    /**
     * Obtenir la commande
     * @return Object Commande
     */
    public Commande getCommande() {
        return commande;
    }
    
    /**
     * Modifier la commande
     * 
     * @param commande Object Commande 
     */
    public void setCommande(Commande commande) {
        this.commande = commande;
    }

    /**
     * Obtenir le produit
     * 
     * @return Produit
     */
    public Produit getProduit() {
        return produit;
    }
    
    /**
     * Modifier le produit
     * @param produit Produit
     */
    public void setProduit(Produit produit) {
        this.produit = produit;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (commandercPK != null ? commandercPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Commanderc)) {
            return false;
        }
        Commanderc other = (Commanderc) object;
        if ((this.commandercPK == null && other.commandercPK != null) || (this.commandercPK != null && !this.commandercPK.equals(other.commandercPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.toulouse.miage.m1.corentinbarban.LaTAIME.entities.Commanderc[ commandercPK=" + commandercPK + " ]";
    }
    
}
